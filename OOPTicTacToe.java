
import java.util.*;
class Board{
	char[][]TicTacToe = new char[3][3];
	
	
	void display(){
		
		System.out.println("\n");
		System.out.println("\t  " + TicTacToe[0][0] + " |   " + TicTacToe[0][1] + "   | " + TicTacToe[0][2]);
		System.out.println("\t____|_______|____");
		System.out.println("\t  " + TicTacToe[1][0] + " |   " + TicTacToe[1][1] + "   | " + TicTacToe[1][2]);
		System.out.println("\t____|_______|____");
		System.out.println("\t  " + TicTacToe[2][0] + " |   " + TicTacToe[2][1] + "   | " + TicTacToe[2][2]);
		System.out.println("\t    |       |    ");
		System.out.println("\n");
	}
	
}
public class OOPTicTacToe extends Board{
	
	String choice2, choice;
	int number, player = 1, select;
	static Scanner scanner = new Scanner(System.in);
	
	void play(){
		char[][]TicTacToe = new char[3][3];
		
		OOPTicTacToe call = new OOPTicTacToe();
			for(int row = 0; row < 3; row++){
				for(int col = 0; col < 3; col++){
					TicTacToe[row][col] = ' ';
					this.TicTacToe[row][col] = TicTacToe[row][col];
				}
			}
			
			
			do{
				do{
					System.out.println("\n");
					
					System.out.println("Choose your mark ( x / o )");
					try{
						choice = scanner.nextLine();
						if(choice.contentEquals("x") == false && choice.contentEquals("o") == false){
							throw new InputMismatchException();
						}
					}catch(InputMismatchException e){
						System.out.println("Please input only the 'x' or 'o' letter");
					}
						if(choice.contentEquals("x")){
							number = 1;
							break;
						}
						if(choice.contentEquals("o")){
							number = 0;
							break;
						}
						
				}while(true);
				
				while(true){
					int threshold = 0;
					
					System.out.println("\n");
					System.out.println("\t  " + TicTacToe[0][0] + " |   " + TicTacToe[0][1] + "   | " + TicTacToe[0][2]);
					System.out.println("\t____|_______|____");
					System.out.println("\t  " + TicTacToe[1][0] + " |   " + TicTacToe[1][1] + "   | " + TicTacToe[1][2]);
					System.out.println("\t____|_______|____");
					System.out.println("\t  " + TicTacToe[2][0] + " |   " + TicTacToe[2][1] + "   | " + TicTacToe[2][2]);
					System.out.println("\t    |       |    ");
					System.out.println("\n");
				
					if(TicTacToe[0][0]  !=' ' && TicTacToe[0][0] == TicTacToe[0][1] && TicTacToe[0][0] == TicTacToe[0][2] ||
						TicTacToe[1][0] !=' ' && TicTacToe[1][0] == TicTacToe[1][1] && TicTacToe[1][0] == TicTacToe[1][2] ||
						TicTacToe[2][0] !=' ' && TicTacToe[2][0] == TicTacToe[2][1] && TicTacToe[2][0] == TicTacToe[2][2] ||
						TicTacToe[0][0] !=' ' && TicTacToe[0][0] == TicTacToe[1][1] && TicTacToe[0][0] == TicTacToe[2][2] ||
						TicTacToe[0][2] !=' ' && TicTacToe[0][2] == TicTacToe[1][1] && TicTacToe[0][2] == TicTacToe[2][0] ||
						TicTacToe[0][0] !=' ' && TicTacToe[0][0] == TicTacToe[1][0] && TicTacToe[0][0] == TicTacToe[2][0] ||
						TicTacToe[0][1] !=' ' && TicTacToe[0][1] == TicTacToe[1][1] && TicTacToe[0][1] == TicTacToe[2][1] ||
						TicTacToe[0][2] !=' ' && TicTacToe[0][2] == TicTacToe[1][2] && TicTacToe[0][2] == TicTacToe[2][2])
					{
						if(player == 1)
							player++;
						else if(player == 2)
							player--;
						System.out.println("GAME OVER, PLAYER "+player+" WINS");
						do{
							try{
								
								System.out.println("Play again? (y/n)");
								choice2 = scanner.nextLine();
								
								if(choice2.contentEquals("y") == false && choice2.contentEquals("n") == false){
									throw new InputMismatchException();
								}
							}
							catch(InputMismatchException e){
								System.out.println("Please input only the 'y' or 'n' letter");
								
								continue;
							}
							break;
						}while(true);
						
						break;
					}
					
					
					for(int row = 0; row < 3; row++){
						for(int col = 0; col < 3; col++){
							if(TicTacToe[row][col] !=' '){
								threshold++;
							}
						}
					}if (threshold == 9){
						System.out.println("GAME OVER, PLAYERS HAVE TIED");
						do{
							try{
								
								System.out.println("Play again? (y/n)");
								choice2 = scanner.nextLine();
								
								if(choice2.contentEquals("y") == false && choice2.contentEquals("n") == false){
									throw new InputMismatchException();
								}
							}
							catch(InputMismatchException e){
								System.out.println("Please input only the 'y' or 'n' letter");
								
								continue;
							}
							break;
						}while(true);
						if(choice2.contentEquals("n")){
							break;
						}else if(choice2.contentEquals("y")){
							break;
						}
					}
					
					System.out.println("\tPlayer "+player+"'s turn");
					System.out.println();
					System.out.println("Input one of the following number to fill the board :");
					if(TicTacToe[0][0] ==' '){
						System.out.println("1. Row 1 Column 1");
					}
					if(TicTacToe[0][1] ==' '){
						System.out.println("2. Row 1 Column 2");
					}
					if(TicTacToe[0][2] ==' '){
						System.out.println("3. Row 1 Column 3");
					}
					if(TicTacToe[1][0] ==' '){
						System.out.println("4. Row 2 Column 1");
					}
					if(TicTacToe[1][1] ==' '){
						System.out.println("5. Row 2 Column 2");
					}
					if(TicTacToe[1][2] ==' '){
						System.out.println("6. Row 2 Column 3");
					}
					if(TicTacToe[2][0] ==' '){
						System.out.println("7. Row 3 Column 1");
					}
					if(TicTacToe[2][1] ==' '){
						System.out.println("8. Row 3 Column 2");
					}
					if(TicTacToe[2][2] ==' '){
						System.out.println("9. Row 3 Column 3");
					}
					do{
						System.out.print(">> ");
						select = -1;
						try{
							select = scanner.nextInt();
							scanner.nextLine();
							break;
						}
						catch(InputMismatchException e){
							System.out.println("Please input only the given number");
							scanner.nextLine();
							select=-1;
							continue;
						}
					}while(true);
					
					switch(select){
					case 1:
						if(TicTacToe[0][0] == ' ' && select == 1){
							if(number == 1){
								TicTacToe[0][0] = 'x';
								this.TicTacToe[0][0]=TicTacToe[0][0];
								number--;
							}
							else if(number == 0){
								TicTacToe[0][0] = 'o';
								this.TicTacToe[0][0] = TicTacToe[0][0];
								number++;
							}
								if(player == 1) player++;
								else if(player == 2) player--;
						}
						else if(TicTacToe[0][0] !=' ' && select == 1){
							System.out.println("Please fill one of the empty space");
						}
						break;
					case 2:
						if(TicTacToe[0][1] == ' ' && select == 2){
							if(number == 1){
								TicTacToe[0][1] = 'x';
								this.TicTacToe[0][1] = TicTacToe[0][1];
								number--;
							}
							else if(number == 0){
								TicTacToe[0][1] = 'o';
								this.TicTacToe[0][1] = TicTacToe[0][1];
								number++;
							}
								if(player == 1) player++;
								else if(player == 2) player--;
						}
						else if(TicTacToe[0][1] !=' ' && select == 2){
							System.out.println("Please fill one of the empty space");
						}
						break;
					case 3:
						if(TicTacToe[0][2] == ' ' && select == 3){
							if(number == 1){
								TicTacToe[0][2] = 'x';
								this.TicTacToe[0][2] = TicTacToe[0][2];
								number--;
							}
							else if(number == 0){
								TicTacToe[0][2] = 'o';
								this.TicTacToe[0][2] = TicTacToe[0][2];
								number++;
							}	
								if(player == 1) player++;
								else if(player == 2) player--;
						}
						else if(TicTacToe[0][2] != ' ' && select == 3){
							System.out.println("Please fill one of the empty space");
						}
					case 4:
						if(TicTacToe[1][0] == ' ' && select == 4){
							if(number == 1){
								TicTacToe[1][0] = 'x';
								this.TicTacToe[1][0] = TicTacToe[1][0];
								number--; 
							}
							else if(number == 0){
								TicTacToe[1][0] = 'o';
								this.TicTacToe[1][0] = TicTacToe[1][0];
								number++;
							}	
								if(player == 1) player++;
								else if(player == 2) player--;
						}
						else if(TicTacToe[1][0] != ' ' && select == 4){
							System.out.println("Please fill one of the empty space");
						}
						break;
					case 5:
						if(TicTacToe[1][1] == ' ' && select == 5){
							if(number == 1){
								TicTacToe[1][1] = 'x';
								this.TicTacToe[1][1] = TicTacToe[1][1];
								number--;
							}
							else if(number == 0){
								TicTacToe[1][1] = 'o';
								this.TicTacToe[1][1] = TicTacToe[1][1];
								number++;
							}	
								if(player == 1) player++;
								else if(player == 2) player--;
						}
						else if(TicTacToe[1][1] != ' ' && select == 5){
							System.out.println("Please fill one of the empty space");
						}
						break;
					case 6:
						if(TicTacToe[1][2] == ' ' && select == 6){
							if(number == 1){
								TicTacToe[1][2] = 'x';
								this.TicTacToe[1][2] = TicTacToe[1][2];
								number--;
							}
							else if(number == 0){
								TicTacToe[1][2] = 'o';
								this.TicTacToe[1][2] = TicTacToe[1][2];
								number++;
							}	
								if(player == 1) player++;
								else if(player == 2) player--;
						}
						else if(TicTacToe[1][2] != ' ' && select == 6){
							System.out.println("Please fill one of the empty space");
						}
						break;
					case 7:
						if(TicTacToe[2][0] == ' ' && select == 7){
							if(number == 1){
								TicTacToe[2][0] = 'x';
								this.TicTacToe[2][0] = TicTacToe[2][0];
								number--;
							}
							else if(number == 0){
								TicTacToe[2][0] = 'o';
								this.TicTacToe[2][0] = TicTacToe[2][0];
								number++;
							}	
								if(player == 1) player++;
								else if(player == 2) player--;
						}
						else if(TicTacToe[2][0] != ' ' && select == 7){
							System.out.println("Please fill one of the empty space");
						}
						break;
					case 8:
						if(TicTacToe[2][1] == ' ' && select == 8){
							if(number == 1){
								TicTacToe[2][1] = 'x';
								this.TicTacToe[2][1] = TicTacToe[2][1];
								number--;
							}
							else if(number == 0){
								TicTacToe[2][1] = 'o';
								this.TicTacToe[2][1] = TicTacToe[2][1];
								number++;
							}
								if(player == 1) player++;
								else if(player == 2) player--;
						}
						else if(TicTacToe[2][1] != ' ' && select == 8){
							System.out.println("Please fill one of the empty space");
						}
						break;
					case 9:
						if(TicTacToe[2][2] == ' ' && select == 9){
							if(number == 1){
								TicTacToe[2][2] = 'x';
								this.TicTacToe[2][2] = TicTacToe[2][2];
								number--;
							}
							else if(number == 0){
								TicTacToe[2][2] = 'o';
								this.TicTacToe[2][2] = TicTacToe[2][2];
								number++;
							}	
								if(player == 1) player++;
								else if(player == 2) player--;
						}
						else if(TicTacToe[2][2] != ' ' && select == 9){
							System.out.println("Please fill one of the empty space");
						}
						break;
					default:
						System.out.println("Please select a correct number");
						break;
					}				
				}
				
				if(choice2.contentEquals ("y") ){
					continue;
				}
				else if(choice2.contentEquals ("n") ){
					break;
				}
				
			}while(true);
			
		
	}
	
	public static void main(String[] args) {
		OOPTicTacToe call = new OOPTicTacToe();
			try{
				do{
					int select=-1;
					
					System.out.println("\n");
					System.out.println("TIC TAC TOE");
					System.out.println("1. Play against your friend");
					System.out.println("2. Play against A.I");
					System.out.println("0. Exit");
					System.out.println("Please select a number");
					try{
						select = scanner.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Please input only the number '1', '2', or '0'");
						scanner.nextLine();
						continue;
					}catch(NoSuchElementException e){
						System.out.println();
						System.out.println("You pressed Ctrl+Z");
						System.out.println("Program Terminated");
						break;
					}
					
					switch(select){
					case 1:
						scanner.nextLine();
						call.play();
						break;
					case 2:
						scanner.nextLine();
						call.play();
						break;
					case 0:
						break;
					default:
					 	System.out.println("\n");
						System.out.println("Please input only the number '1', '2', or '0");
						break;
					}
					if (select==0){
						break;
					}
				}while(true);
			
		}catch(NoSuchElementException e){
			System.out.println("\n");
			System.out.println("You pressed Ctrl+Z");
			System.out.println("Program Terminated");
		}
    }	
}