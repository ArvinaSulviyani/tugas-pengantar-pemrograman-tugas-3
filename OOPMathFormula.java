import java.io.*;
import java.util.*;


class OOP extends Main{
	public static void main(String[]args){
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		
		Main mainMenu = new Main();
		
		try{
			mainMenu.display();
		}catch(NumberFormatException e){
			System.out.println();
			System.out.println("You pressed Ctrl + Z");
			System.out.println("Program Terminated");
		}
	}	
}

class Main{	
	static BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
	String inputData = null;
    int choice = 0;
	void display(){
		
		Square Square = new Square();
        Rectangle Rectangle = new Rectangle();
        Triangle Triangle = new Triangle();
        Parallelogram  Parallelogram = new Parallelogram();
    	Trapezoida Trapezoida = new Trapezoida();
    	Kite Kite = new Kite();
    	Rhombus Rhombus = new Rhombus();
    	Circle Circle = new Circle();
		Cube Cube = new Cube();
		Cuboid Cuboid = new Cuboid();
		Pyramid Pyramid = new Pyramid();
		Prism Prism = new Prism();
		Cylinder Cylinder = new Cylinder();
    	Sphere Sphere = new Sphere();
    	Cone Cone = new Cone();
    	
    	
	        do {
	        	System.out.println("\nMenu Rumus");
	        	System.out.println("1.  Persegi");
	        	System.out.println("2.  Persegi Panjang");
	        	System.out.println("3.  Segitiga Siku - Siku");
	        	System.out.println("4.  Jajargenjang");
	        	System.out.println("5.  Trapesium");
	        	System.out.println("6.  Layang - Layang");
	        	System.out.println("7.  Belah Ketupat");
	        	System.out.println("8.  Lingkaran");
	        	System.out.println("9.  Kubus");
	        	System.out.println("10. Balok");
	        	System.out.println("11. Limas");
	        	System.out.println("12. Prisma");
	        	System.out.println("13. Tabung");
	        	System.out.println("14. Bola");
	        	System.out.println("15. Kerucut");
	        	System.out.println("0.  Keluar");
	            System.out.print("Masukkan Pilihan Anda : ");
	            try {
	                inputData = bufferedReader.readLine();
	                try  {
	                    choice = Integer.parseInt(inputData);
	                    if (choice > 0 && choice == 1) {
	                        Square.calculate();
	                    }
	                    else if (choice > 0 && choice == 2) {
	                        Rectangle.calculate();
	                    }
	                    else if (choice > 0 && choice == 3) {
	                        Triangle.calculate();
	                    }
	                    else if (choice > 0 && choice == 4) {
	                        Parallelogram.calculate();
	                    }
	                    else if (choice > 0 && choice == 5) {
	                    	Trapezoida.calculate();
	                    }
	                    else if (choice > 0 && choice == 6) {
	                    	Kite.calculate();
	                    }
	                    else if (choice > 0 && choice == 7) {
	                    	Rhombus.calculate();
	                    }
	                    else if (choice > 0 && choice == 8) {
	                    	Circle.calculate();
	                    }
	                    else if (choice > 0 && choice == 9) {
	                    	Cube.calculate();
	                    }
	                    else if (choice > 0 && choice == 10) {
	                    	Cuboid.calculate();
	                    }
	                    else if (choice > 0 && choice == 11) {
	                    	Pyramid.calculate();
	                    }
	                    else if (choice > 0 && choice == 12) {
	                    	Prism.calculate();
	                    }
	                    else if (choice > 0 && choice == 13) {
	                    	Cylinder.calculate();
	                    }
	                    else if (choice > 0 && choice == 14) {
	                    	Sphere.calculate();
	                    }
	                    else if (choice > 0 && choice == 15) {
	                    	Cone.calculate();
	                    }
	                    else {
	                        System.out.println("TERIMA KASIH");
	                    }
	                }

	                catch(NumberFormatException e) {

	                    System.out.println("Masukan Anda Tidak Sesuai");

	                }

	            }

	            catch (IOException error) {

	                System.out.println("Error Input " + error.getMessage());

	            }


	        } while(choice > 0);
	}
}

class SquareFormula{
	float side;
	double wide(){
		return side * side;
	}
    double perimeter(){
    	return 4 * side;
    }
}

	class Square extends SquareFormula{
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		  String inputData = null;
		  
		  void calculate(){
		  float side;
		  System.out.print("Masukan Panjang Sisi : ");
		  try {
		
			  inputData = bufferedReader.readLine();
		        }
		     catch (IOException error) {
		        System.out.println("Error Input " + error.getMessage());
		 }

		 try  {
		        side = Float.parseFloat(inputData);
		        this.side = side;
		    
		        System.out.println("Luas Persegi: " + wide());
		        
		        System.out.println("Keliling Persegi : " + perimeter());
		        }
		        catch(NumberFormatException e) {
		            System.out.println("Masukan Anda Tidak Sesuai");
		        }
		    }
	}
	
class RectangleFormula{
	float length, width;
	double wide(){
		return length * width;
	}
    double perimeter(){
    	return ((2 * length) + (2 * width));
    }
}

	class Rectangle extends RectangleFormula{
		
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLength = null;
        
        void calculate(){
        float width, length;
        System.out.print("Masukan Panjang Sisi : ");
        try {
        	
        	inputDataLength = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Lebar Sisi : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataWidth = null;
        try {
        	
        	inputDataWidth = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        try  {
            length = Float.parseFloat(inputDataLength);
            this.length = length;
            width = Float.parseFloat(inputDataWidth);
            this.width = width;
            float wide = length * width;
            System.out.println("Luas Persegi Panjang: " + wide);
            
            float perimeter = ((2 * length) + (2 * width));
            System.out.println("Keliling Persegi Panjang : " + perimeter);
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Anda Tidak Sesuai");
        }
        }
	}
	
class TriangleFormula{
	float side, high;
	 double wide(){
		 return 0.5 * high * side;
	 }
     double perimeter(){
    	 double hypotenuse = (Math.sqrt(((high * high) + (side * side))));
    	 return high + side + hypotenuse;
     }
}

	class Triangle extends TriangleFormula{
		
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHigh = null;
        
        void calculate(){
        float high, side;
        System.out.print("Masukan Tinggi Segitiga : ");
        try {
        	
        	inputDataHigh = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        System.out.print("Masukan Sisi Segitiga : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataSide = null;
        try {
        	
        	inputDataSide = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }

        try  {
            high = Float.parseFloat(inputDataHigh);
            this.high = high;
            side = Float.parseFloat(inputDataSide);
            this.side = side;
           
            System.out.println("Luas Segitiga : " + wide());
            
            System.out.println("Keliling Segitiga Siku - Siku : " + perimeter());
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Anda Tidak Sesuai");
        }
    }
	}
	
class ParallelogramFormula{
	float length, high, hypotenuse;
	double wide(){
		return length * high;
	}
    double perimeter(){
    	return ((2 *length)  + (2 *hypotenuse));
    }
}

	class Parallelogram extends ParallelogramFormula{
		
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHigh = null;
        
        void calculate(){
        float length, high, hypotenuse;
        System.out.print("Masukan Tinggi Jajargenjang : ");
        try {
        	
        	inputDataHigh = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Panjang Sisi Jajargenjang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLength = null;
        try {
        	
        	inputDataLength = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Panjang Sisi Miring Jajargenjang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHypotenuse = null;
        try {
        	
        	inputDataHypotenuse = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try  {
            length = Float.parseFloat(inputDataHigh);
            this.length = length;
            high = Float.parseFloat(inputDataHigh);
            this.high = high;
            hypotenuse = Float.parseFloat(inputDataHypotenuse);
            this.hypotenuse = hypotenuse;
            
            System.out.println("Luas Jajagenjang : " + wide());
            
            System.out.println("Keliling Jajargenjang : " + perimeter());
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Anda Tidak Sesuai");
        }
        }
	}
	
class TrapezoidaFormula{
	float length, width, high;
	double wide(){
		return 0.5 * (length + width) * high;
	}
    double perimeter(){
    	double hypotenuse = (Math.sqrt((((length - width) * (length - width)) + (high * high))));
    	return length + width + hypotenuse + high;
    }
}

	class Trapezoida extends TrapezoidaFormula{
		
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHigh = null;
        
        void calculate(){
        float length, high, width;
        System.out.print("Masukan Tinggi Trapesium : ");
        try {
        	
        	inputDataHigh = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Lebar Sisi Trapesium : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataWidth = null;
        try {
        	
        	inputDataWidth = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.print("Error Input " + error.getMessage());
        }
        
        System.out.println("Masukan Panjang Sisi Trapesium : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLength = null;
        try {
        	
        	inputDataLength = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try  {
            high = Float.parseFloat(inputDataHigh);
            this.high = high;
            width = Float.parseFloat(inputDataWidth);
            this.width = width;
            length = Float.parseFloat(inputDataLength);
            this.high = high;
            System.out.println("Luas Trapesium : " + wide());
            
            System.out.println("Keliling Trapesium : " + perimeter());
        }
        catch(NumberFormatException e) {
            System.out.println("Masukan Anda Tidak Sesuai");
        }
        }
	}
	
class KiteFormula{
	float diagonal1, diagonal2, length, width;
	double wide(){
		return 0.5 * diagonal1 * diagonal2;
	}
	double perimeter(){
		return (2 * (length + width));
	}
}

	class Kite extends KiteFormula{
		
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDiagonal1 = null;
        
        void calculate(){
        float diagonal1, diagonal2, length, width;
        System.out.print("Masukan Panjang Diagonal 1 : ");
        try {
        	
        	inputDataDiagonal1 = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Panjang Diagonal 2 : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDiagonal2 = null;
        try {
        	
        	inputDataDiagonal2 = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.print("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Panjang Sisi Layang - Layang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataLength = null;
        try {
        	
        	inputDataLength = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Lebar Sisi Layang - Layang : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataWidth = null;
        try {
        	
        	inputDataWidth = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try {
        	diagonal1 = Float.parseFloat(inputDataDiagonal1);
        	this.diagonal1 = diagonal1;
        	diagonal2 = Float.parseFloat(inputDataDiagonal2);
        	this.diagonal2 = diagonal2;
        	length = Float.parseFloat(inputDataLength);
        	this.length = length;
        	width = Float.parseFloat(inputDataWidth);
        	this.width = width;
        	
        	System.out.println("Luas Layang - Layang : " + wide());
        	
        	System.out.println("Keliling Layang - Layang : " + perimeter());
        }
        
        catch (NumberFormatException e){
        	System.out.println("Masukan tidak sesuai");
        }
        }
	}
	
class RhombusFormula{
	float diagonal1, diagonal2, side;
	double wide(){
		return  0.5 * diagonal1 * diagonal2;
	}
	double perimeter(){
		return 4 * side;
	}
}

	class Rhombus extends RhombusFormula{
		
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDiagonal1 = null;
        
        void calculate(){
        float diagonal1, diagonal2, side;
        System.out.print("Masukan Diagonal Sisi 1 : ");
        try {
            inputDataDiagonal1 = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
    	System.out.print("Masukan Diagonal Sisi 2 : ");
    	bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataDiagonal2 = null;
        try {
            inputDataDiagonal2 = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Panjang Sisi Belah Ketupat : ");
    	bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataSide = null;
        try {
            inputDataSide = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try {
        	diagonal1 = Float.parseFloat(inputDataDiagonal1);
        	this.diagonal1 = diagonal1;
        	diagonal2 = Float.parseFloat(inputDataDiagonal2);
        	this.diagonal2 = diagonal2;
        	side = Float.parseFloat(inputDataSide);
        	this.side = side;
        	
        	System.out.println("Luas Belah Ketupat : " + wide());
        	
        	System.out.println("Keliling Belah Ketupat : " + perimeter());
        }
        catch(NumberFormatException e){
        	System.out.print("Masukan tidak sesuai");
        }
        }
	}
	
class CircleFormula{
	float radius;
	double wide(){
		return (3.14 * (radius * radius));
	}
    double circumference(){
    	return 2 * 3.14 * radius;
    }
}

	class Circle extends CircleFormula{
		
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataRadius = null;
        
        void calculate(){
        float radius;
        System.out.print("Masukan Jari - Jari Lingkaran : ");
        try {
            inputDataRadius = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try  {
            radius = Float.parseFloat(inputDataRadius);
            this.radius = radius;
            
            System.out.println("Luas Lingkran: " + wide());
            
            System.out.println("Keliling Lingkaran : " + circumference());
            }
            catch(NumberFormatException e) {
                System.out.println("Masukan Anda Tidak Sesuai");
            }
    }
	}
class CubeFormula{
	float side;
	double volume(){
		return side * side * side;
	}
	double surface(){
		return side * side * 6;
	}
}

	class Cube extends CubeFormula{
		 
		static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	        String inputDataSide = null;
			
	        void calculate(){
			float side;
			System.out.print("Masukkan Panjang Kubus : ");
	        try {
	        	
	            inputDataSide = bufferedReader.readLine();
	           
	        }
	        catch (IOException error) {
	            System.out.println("Error Input " + error.getMessage());
	        }
	        
	        try  {
	        	
	           side = Float.parseFloat(inputDataSide);
	           this.side = side;
	            
	            System.out.println("Volume Kubus : " + volume());
	            
	           
	            System.out.println("Luas Permukaan Kubus : " + surface());
	            }
	            catch(NumberFormatException e) {
	                System.out.println("Masukan Anda Tidak Sesuai");
	            }
	    }
	   
	}
	
class CuboidFormula{
	float length, width, high;
	double volume(){
		return length * width * high;
	}
	double surface(){
		return (2 * length * width) + (2 * length * high) + (2 * width * high);
	}
}


	class Cuboid extends CuboidFormula{
		
		    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		    String inputDataLength = null;
		    
		    void calculate(){
		    float length, width, high;
		    System.out.print("Masukkan Panjang : ");
		    try {
		    	
		        inputDataLength = bufferedReader.readLine();
		    }
		    catch (IOException error) {
		        System.out.println("Error Input " + error.getMessage());
		    }
		    
		    System.out.print("Masukan Lebar Balok : ");
		    bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		    String inputDataWidth = null;
		    try {
		    	
		        inputDataWidth = bufferedReader.readLine();
		    }
		    catch (IOException error) {
		        System.out.println("Error Input " + error.getMessage());
		    }
		    
		    System.out.print("Masukan Tinggi Balok : ");
		    bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		    String inputDataHigh = null;
		    try {
		    	
		        inputDataHigh = bufferedReader.readLine();
		    }
		    catch (IOException error) {
		        System.out.println("Error Input " + error.getMessage());
		    }
		    
		    try {
		    	length = Float.parseFloat(inputDataLength);
		    	this.length = length;
		    	width = Float.parseFloat(inputDataWidth);
		    	this.width = width;
		    	high = Float.parseFloat(inputDataHigh);
		    	this.high = high;
		    	
		    	System.out.println("Volume Balok : " + volume());
		    	
		    	System.out.println("Luas Permukaan Balok : "+ surface());
		    }
		    	catch(NumberFormatException e) {
		        System.out.println("Masukan Anda Tidak Sesuai");
		    	}
		}
	}
	
class PyramidFormula{
	float high, length;
	double volume(){
		return ((high * length * length)/3);
	}
	double surface(){
		double trianglehigh = ((high * high) + ((0.5 * length) * (0.5 * length)));
		return ((4 * (0.5 * length * trianglehigh)) + (length * length ));
	}
}

	class Pyramid extends PyramidFormula{
		
    	BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
    	String inputDataLength = null;
    	
    	void calculate(){
		float length, high;
		System.out.print("Masukkan Panjang Sisi Limas Segiempat : ");
    	try {
    		
    		inputDataLength = bufferedReader.readLine();
    	}
    	catch(IOException error){
    		System.out.println("Error Input "+ error.getMessage());
    	}
    	
    	System.out.print("Masukkan Tinggi Limas Segiempat : ");
    	bufferedReader = new BufferedReader (new InputStreamReader(System.in));
    	String inputDataHigh = null;
    	try {
    		
    		inputDataHigh = bufferedReader.readLine();
    	}
    	catch(IOException error){
    		System.out.println("Error Input "+ error.getMessage());
    	}
    	
    	try {
    		length = Float.parseFloat(inputDataLength);
    		this.length = length;
    		high = Float.parseFloat(inputDataHigh);
    		this.high = high;
    		
    		System.out.println("Volume Limas Segiempat : " + volume());
    		
    		System.out.println("Luas Permukaan Limas Segiempat : " + surface());
    	}
    		catch(NumberFormatException e){
    			System.out.println("Masukan tidak sesuai");
    		}
    	
    	}
	}
	
class PrismFormula{
	float side, triangleHigh, high;
	double volume(){
		return  0.5 * side * triangleHigh * high;
	}
	double surface(){
		double hypotenuse = (Math.sqrt(((triangleHigh * triangleHigh) + (0.5 * (side * side)))));
	    double perimeter = (side + (2 * hypotenuse));
		return ((2 * (0.5 * side * triangleHigh)) + (perimeter * high));
	}
}

	class Prism extends PrismFormula{
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataSide = null;
        
        void calculate(){
        float side, triangleHigh, high;
        System.out.print("Masukan Panjang Sisi Segitiga : ");
        try {
        	
            inputDataSide = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.println("Masukan Tinggi Segitiga : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataTriangleHigh = null;
        try {
        	
            inputDataTriangleHigh = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Tinggi Prisma : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHigh = null;
        try {
        	
        	inputDataHigh = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try {
        	side = Float.parseFloat(inputDataSide);
        	this.side = side;
        	high = Float.parseFloat(inputDataHigh);
        	this.high = high;
        	triangleHigh = Float.parseFloat(inputDataTriangleHigh);
        	this.triangleHigh = triangleHigh;
        	
        	System.out.println("Volume Prisma : " + volume());
        	
        	System.out.println("Luas Permukaan Prisma : " + surface());
        }
        catch(NumberFormatException e){
        	System.out.println("Masukan tidak sesuai");
        }
    }
	}
	
class CylinderFormula{
	float radius, high;
	double volume(){
		return  (3.14 * (radius * radius) * high);
	}
	double surface(){
		return ((2 * 3.14 * radius * high) + (2 * 3.14 * (radius * radius)));
	}
}

	class Cylinder extends CylinderFormula{
    	
    	BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
    	String inputDataRadius = null;
    	
    	void calculate(){
    	float radius, high;
    	System.out.print("Masukkan Jari - Jari Lingkaran : ");
    	try {
    		
    		inputDataRadius = bufferedReader.readLine();
    	}
    	catch(IOException error){
    		System.out.println("Error Input "+ error.getMessage());
    	}
    	
    	System.out.print("Masukkan Tinggi Tabung : ");
    	bufferedReader = new BufferedReader (new InputStreamReader(System.in));
    	String inputDataHigh = null;
    	try {
    		
    		inputDataHigh = bufferedReader.readLine();
    	}
    	catch(IOException error){
    		System.out.println("Error Input "+ error.getMessage());
    	}
    	
    	try {
    		radius = Float.parseFloat(inputDataRadius);
    		this.radius = radius;
    		high = Float.parseFloat(inputDataHigh);
    		this.high = high;
    		System.out.println("Volume Tabung : " + volume());
    		
    		System.out.println("Luas Permukaan Tabung : " + surface());
    	}
    		catch(NumberFormatException e){
    			System.out.println("Masukan tidak sesuai");
    		}
    	
    	}	
    
	}
	
class SphereFormula{
	float radius;
	double volume(){
		return (4/3 * 3.14 * radius * radius * radius);
	}
	double surface(){
		return 4 * 3.14 * radius * radius;
	}
}

	class Sphere extends SphereFormula{
		
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataRadius = null;
        
        void calculate(){
        float radius;
        System.out.print("Masukan Jari - Jari Lingkaran : ");
        try {
        	
        	inputDataRadius = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        try {
        	radius = Float.parseFloat(inputDataRadius);
        	this.radius = radius;
        	
        	System.out.println("Volume Bola : " + volume());
        	
        	System.out.println("Luas Permukaan Bola : " + surface());
        }
        catch(NumberFormatException e){
        	System.out.println("Masukan tidak sesuai");
        }
    	}
	}
	
class ConeFormula{
	float radius, high;
	double volume(){
		return ((3.14 * radius * radius * high)/3);
	}
	
	double surface(){
		double hypotenuse = (Math.sqrt(((high * high) + (radius * radius))));
		return ((3.14 * radius * hypotenuse) + (3.14 * radius * radius));
	}
}

	class Cone extends ConeFormula{
		
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataRadius = null;
        
        void calculate(){
        float radius, high;
        System.out.print("Masukan Jari - Jari Lingkaran : ");
        try {
        	
        	inputDataRadius = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        
        System.out.print("Masukan Tinggi Kerucut : ");
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputDataHigh = null;
        try {
        	
        	inputDataHigh = bufferedReader.readLine();
        }
        catch (IOException error) {
            System.out.println("Error Input " + error.getMessage());
        }
        try {
        	radius = Float.parseFloat(inputDataRadius);
        	this.radius = radius;
        	high = Float.parseFloat(inputDataHigh);
        	this.high = high;
        	
        	System.out.println("Volume Kerucut : " + volume());
        
        	System.out.println("Luas Permukaan Kerucut : " + surface());
        }
        catch(NumberFormatException e){
        	System.out.println("Masukan tidak sesuai");
        }
        }
	}
	