
import java.util.*;
public class OOPMatrix extends Matrix{
	static Scanner scanner = new Scanner(System.in);
	
	 void MainMenu(){
		 OOPMatrix call = new OOPMatrix();
		while(true){
			
		
			int select;
			System.out.println("1. Penjumlahan Matriks");
			System.out.println("2. Pengurangan Matriks");
			System.out.println("3. Perkalian Matriks");
			System.out.println("4. Determinan Matriks");
			System.out.println("0. Keluar");	
			System.out.println("Silahkan Pilih Nomor Yang Anda Inginkan : ");
			do{
				System.out.print(">> ");
				try{
					select = scanner.nextInt();
					if(select!= 1 && select!= 2 && select!= 3  && select!= 0 ){
						System.out.println("Please select only the '1', '2', '3', or '0' number");
						continue;
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Please select only the '1', '2', '3', or '0' number");
					scanner.nextLine();
					continue;
				}
			}while(true);
			
			if(select== 0){
				break;
			}
			
			switch(select){
				case 1:
					call.matrixAddition();
					break;
				case 2:
					call.matrixSubtraction();
					break;
				case 3:
					call.matrixMultiplication();
					break;
				
			}
		}
	}
	
	void matrixAddition(){
			int row, column;
			do{
				System.out.println("\t  Penjumlahan Matriks");
				do{	
					try{
						System.out.print("Input Nilai Baris Matriks : ");
						row = scanner.nextInt();
							if (row < 1){
								throw new InputMismatchException();
							}
						System.out.print("Input Nilai Kolom Matriks : ");
						column = scanner.nextInt();
							if (column < 1){
								throw new InputMismatchException();
							}
						break;
					}catch(InputMismatchException e){
						System.out.println("Maaf Inputan Anda Salah!!! Silahkan Input Bilangan Asli ( > 0 )");
						scanner.nextLine();
						continue;
					}
				}while(true);
				
				double[][]matrix1 = new double[row][column];
				double[][]matrix2 = new double[row][column];
				double[][]matrixResult = new double[row][column];
				System.out.println("MATRIKS 1");
				do{
					try{
						for(int indexRow = 0; indexRow < row; indexRow++){
							for(int indexColumn = 0; indexColumn < column; indexColumn++){
								System.out.print("Input Nilai Baris ke-" + (indexRow + 1) + " dan kolom ke-" + (indexColumn + 1) + " : " );
								matrix1[indexRow][indexColumn] = scanner.nextDouble();
							}
						}
						break;
					}catch(InputMismatchException e){
						System.out.println("Silahkan Input Bilangan Riil");
						scanner.nextLine();
						continue;
					}	
				}while(true);
				
				System.out.println();
				for(int indexRow = 0; indexRow < row; indexRow++){
					for(int indexColumn = 0; indexColumn < column; indexColumn++){
						
						System.out.printf("%.2f  ",matrix1[indexRow][indexColumn]);
					}System.out.println();
				}
				
				
				System.out.println();
				System.out.println("MATRIKS 2");
				do{
					try{
						for(int indexRow = 0; indexRow < row; indexRow++){
							for(int indexColumn = 0; indexColumn < column; indexColumn++){ 
								System.out.print("Input Nilai Baris ke-"+ (indexRow + 1) +" dan kolom ke-"+ (indexColumn + 1) + " : ");
								matrix2[indexRow][indexColumn] = scanner.nextDouble(); 
								matrixResult[indexRow][indexColumn] = matrix1[indexRow][indexColumn] + matrix2[indexRow][indexColumn];
							}
						}
						break;
					}catch(InputMismatchException e){
						System.out.println("Silahkan Input Bilangan Riil");
						scanner.nextLine();
						continue;
					}	
				}while(true);
				
				System.out.println();
				for(int indexRow = 0; indexRow < row; indexRow++){
					for(int indexColumn = 0; indexColumn < column; indexColumn++){
						
						System.out.printf("%.2f  ",matrix2[indexRow][indexColumn]);
					}System.out.println();
				}
				
				System.out.println("\n");
				System.out.println("Hasil dari Penjumlahan Dua Matriks di atas adalah : ");
				System.out.println();
				for(int indexRow = 0; indexRow < row; indexRow++){
					for(int indexColumn = 0; indexColumn < column; indexColumn++){
						this.matrix1 = matrix1[indexRow][indexColumn];
						this.matrix2 = matrix2[indexRow][indexColumn];
						System.out.printf("%.3f  ", add());
					}System.out.println();
				}
				System.out.println();
				String choice;
				scanner.nextLine();
				do{
					System.out.println("Coba Lagi ? (yes/no)");
					choice = scanner.nextLine();
					if (choice.contentEquals("ye")){
						 break;
					 }
					 else  if (choice.contentEquals("no")){
						 break;
					 }
					 else{
						 System.out.println("Maaf, Anda Hanya Bisa Input Kata 'yes' atau 'no'");
						 continue;
					 }
				}while(true);
				 if (choice.contentEquals("no")){
					 break;
				 }
				 else  if (choice.contentEquals("yes")){
					 continue;
				 }
			}while(true);		
		}
	
		void matrixSubtraction(){
			int row, column;
			do{
				System.out.println("\t  Pengurangan Matriks");
				do{	
					try{
						
						System.out.print("Input Nilai Baris Matriks : ");
						row = scanner.nextInt();
							if (row < 1){
								throw new InputMismatchException();
							}
						System.out.print("Input Nilai Kolom Matriks : ");
						column = scanner.nextInt();
							if (column <1){
								throw new InputMismatchException();
							}
						break;
					}catch(InputMismatchException e){
						System.out.println("Maaf Inputan Anda Salah!!! Silahkan Input Bilangan Asli ( > 0 )");
						scanner.nextLine();
						continue;
					}
				}while(true);
				
				double[][]matrix1 = new double[row][column];
				double[][]matrix2 = new double[row][column];
				double[][]matrixResult = new double[row][column];
				System.out.println("MATRIKS 1");
				do{
					try{
						for(int indexRow = 0; indexRow < row; indexRow++){
							for(int indexColumn = 0; indexColumn < column; indexColumn++){
								System.out.print("Input Nilai Baris Ke-" + (indexRow + 1) + " dan Kolom ke-" + (indexColumn + 1) + " : ");
								matrix1[indexRow][indexColumn] = scanner.nextDouble();
							}
						}
						break;
					}catch(InputMismatchException e){
						System.out.println("Silahkan Input Bilangan Riil");
						scanner.nextLine();
						continue;
					}	
				}while(true);
				
				System.out.println();
				for(int indexRow = 0; indexRow < row; indexRow++){
					for(int indexColumn = 0; indexColumn < column; indexColumn++){		
						System.out.printf("%.3f  ", matrix1[indexRow][indexColumn]);
					}System.out.println();
				}
				System.out.println();
				System.out.println("MATRIKS 2");
				do{
					try{
						for(int indexRow = 0; indexRow < row; indexRow++){
							for(int indexColumn = 0; indexColumn < column; indexColumn++){
								System.out.print("Input Nilai Baris ke-"+ (indexRow + 1) +" dan Kolom ke-" + (indexColumn + 1) + " : ");
								matrix2[indexRow][indexColumn] = scanner.nextDouble();
								matrixResult[indexRow][indexColumn] = matrix1[indexRow][indexColumn] - matrix2[indexRow][indexColumn];
							}
						}
						break;
					}catch(InputMismatchException e){
						System.out.println("Silahkan Input Bilangan Riil");
						scanner.nextLine();
						continue;
					}	
				}while(true);
				
				System.out.println();
				for(int indexRow = 0; indexRow < row; indexRow++){
					for(int indexColumn = 0; indexColumn < column; indexColumn++){
						System.out.printf("%.3f  ", matrix2[indexRow][indexColumn]);
					}System.out.println();
				}
				
				System.out.println("\n");
				System.out.println("Hasil dari Pengurangan Dua Matriks di atas adalah  : ");
				System.out.println();		
				
				for(int indexRow = 0; indexRow < row; indexRow++){
					for(int indexColumn = 0; indexColumn < column; indexColumn++){
						this.matrix1 = matrix1[indexRow][indexColumn];
						this.matrix2 = matrix2[indexRow][indexColumn];
						System.out.printf("%.3f  ", matrixResult[indexRow][indexColumn]);
					}System.out.println();
				}				
				System.out.println();
				String choice;
				scanner.nextLine();
				do{
					System.out.println("Coba Lagi ? (yes/no)");
					choice = scanner.nextLine();
					if (choice.contentEquals("no")){
						 break;
					 }
					 else  if (choice.contentEquals("yes")){
						 break;
					 }
					 else{
						 System.out.println("Maaf, Anda Hanya Bisa Input Kata 'yes' atau 'no'");
						 continue;
					 }
				}while(true);

					 if (choice.contentEquals("no")){
						 break;
					 }
					 else  if (choice.contentEquals("yes")){
						 continue;
					 }
			}while(true);
		}
		
		void matrixMultiplication(){
			
			while(true){
				 int w = 0, choice = 0, row1, column1, row2, column2;
			 do{
				 do{
					 try{
						 System.out.print("Baris Matriks 1 : ");
						 row1 = scanner.nextInt();
						 System.out.print("Kolom Matriks 1 : ");
						 column1 = scanner.nextInt();
						 System.out.print("Baris Matriks 2 : ");
						 row2 = scanner.nextInt();
						 System.out.print("Kolom Matriks 2 : ");
						 column2 = scanner.nextInt();
						 break;
					 }catch(InputMismatchException e){
						 System.out.println("Silahkan Masukkan Bilangan Bulat");
						 scanner.nextLine();
						 continue;
					 }
				 }while(true);
				
						if (column1 != row2) {
							System.out.println("Jumlah Baris matriks 1 Tidak Sama Dengan Jumlah Kolom matriks 2");
							System.out.println("Matriks Tidak Dapat Di kalikan");					
							do{
								try{
									System.out.print("tekan '0' untuk mengulang Proses atau '1' untuk berhenti : ");
									choice = scanner.nextInt();
									if (choice == 1)break;
									if (choice == 0)break;
								}catch(InputMismatchException e){
									System.out.println("Input Satu Angka Yang Benar");
									scanner.nextLine();
									continue;
								}
							}while((choice!=0)||(choice!=1));
						}			
						if (choice == 1)break;
						if (choice == 0)continue;
			 }while(column1 != row2);
				 if (choice == 1)break;
				 System.out.println("\n");
				 
			int [][] matrix = new int[row1][column1];
			do{
				System.out.println("Matriks 1: ");
				for(int indexRow = 0; indexRow < row1; indexRow++){
					for(int indexColumn = 0; indexColumn < column1; indexColumn++){
						System.out.print("Baris " + (indexRow + 1) + " Kolom " + (indexColumn + 1) + " : ");
						try{
							matrix[indexRow][indexColumn] = scanner.nextInt();
						}catch(InputMismatchException e){
							System.out.println("Maaf Anda Hanya Dapat Input Bilangan Bulat");
							scanner.nextLine();
							indexColumn--;
							continue;
						}
					}
				}
				break;
			}while(true);
			
			
			System.out.println();
			
			for(int indexRow = 0; indexRow < row1; indexRow++){
				for(int indexColumn = 0; indexColumn < column1; indexColumn++){
					System.out.printf("%4d ", matrix[indexRow][indexColumn]);
				}System.out.println();
			}System.out.println();
			
			int [][] matrix2 = new int[row2][column2];
			do{
				System.out.println("Matriks 2: ");
				for(int indexRow = 0; indexRow < row2; indexRow++){
					for(int indexColumn = 0; indexColumn < column2; indexColumn++){
						System.out.print("Baris " + (indexRow + 1) + " Kolom " + (indexColumn + 1) + " : ");
						try{
							matrix2[indexRow][indexColumn] = scanner.nextInt();
						}catch(InputMismatchException e){
							System.out.println("Maaf Anda Hanya Dapat Input Bilangan Bulat");
							scanner.nextLine();
							indexColumn--;
							continue;
						}
						
					}
				}
				break;
			}while(true);
		
			System.out.println();
				
			for(int indexRow = 0; indexRow < row2; indexRow++){
				for(int indexColumn = 0; indexColumn < column2; indexColumn++){
					
					System.out.printf("%4d ", matrix2[indexRow][indexColumn]);
				}
				System.out.println();
			}
					
			int [][] matrix3 = new int[row1][column2];
			for(int indexRow = 0; indexRow < row1; indexRow++){
				for(int indexColumn = 0; indexColumn < column2; indexColumn++){
					w = 0;
					for(int indexMultiplication = 0; indexMultiplication < column1; indexMultiplication++){
						w += matrix[indexRow][indexMultiplication] * matrix2[indexMultiplication][indexColumn];
					}
					matrix3[indexRow][indexColumn] = w;
						}
					}System.out.println();
					
					
				
			System.out.print("Perkalian Matriks (" + row1 + "x" + column1 + ") X (" + row2 + "x" + column2 + ") menghasilkan Matriks " + row1 + "x" + column2 + " : ");
			System.out.println("Hasil dari Perkalian Dua Matriks di atas adalah :\n");
			for(int indexRow = 0; indexRow < row1; indexRow++){
				for(int indexColumn = 0; indexColumn < column2; indexColumn++){
					this.multiply = matrix3[indexRow][indexColumn];
					System.out.printf("%4d ", matrix3[indexRow][indexColumn]);
				}System.out.println();
				}System.out.println("\n");				
				
				do{
					try{
						System.out.print("tekan '0' untuk mengulang Proses atau '1' untuk berhenti : ");
						choice = scanner.nextInt();
						if (choice == 0)break;
						if (choice == 1)break;
					}catch(InputMismatchException e){
						System.out.println("Input Satu Angka Yang Benar");
						scanner.nextLine();
						continue;
					}
					
				}while((choice < 0) || (choice > 1));
				
				if (choice == 0)break;
				if(choice == 1)continue;
				System.out.println();
			 }
		}
		
		public static void matrixDeterminant(){
			int row, column;
			do{
				System.out.println("\t  Determinan Matriks");
				
				double[][]matrix1= new double[2][2];
				double[][]matrix2= new double[2][2];
				double[][]matrixResult=new double[2][2];
				System.out.println("MATRIKS 1");
				do{
					try{
						for(int indexRow = 0; indexRow < 2; indexRow++){
							for(int indexColumn = 0; indexColumn < 2; indexColumn++){
								System.out.print("Input Nilai Baris ke-" + (indexRow + 1) + " dan kolom ke-" + (indexColumn + 1) + " : " );
								matrix1[indexRow][indexColumn] = scanner.nextDouble();
							}
						}
						break;
					}catch(InputMismatchException e){
						System.out.println("Silahkan Input Bilangan Riil");
						scanner.nextLine();
						continue;
					}	
				}while(true);
				
				System.out.println();
				for(int indexRow = 0; indexRow < 2; indexRow++){
					for(int indexColumn = 0; indexColumn < 2; indexColumn++){
						
						System.out.printf("%.2f  ",matrix1[indexRow][indexColumn]);
					}System.out.println();
				}
				
				double determinant1 = (matrix1[0][0]*matrix1[1][1])-(matrix1[1][0]*matrix1[0][1]);
				System.out.println("Determinan dari Matriks 1 adalah " + determinant1);
				if (determinant1 == 0) {
					System.out.println("Ini merupakan Matriks Pencerminan");
				}
				else {
					System.out.println("Matriks ini Bukanlah Matriks Pencerminan");
				}
				
				System.out.println();
				System.out.println("MATRIKS 2");
				do{
					try{
						for(int indexRow = 0; indexRow < 2; indexRow++){
							for(int indexColumn = 0; indexColumn < 2; indexColumn++){ 
								System.out.print("Input Nilai Baris ke-"+ (indexRow + 1) +" dan kolom ke-"+ (indexColumn + 1) + " : ");
								matrix2[indexRow][indexColumn] = scanner.nextDouble(); 
								matrixResult[indexRow][indexColumn] = matrix1[indexRow][indexColumn] + matrix2[indexRow][indexColumn];
							}
						}
						break;
					}catch(InputMismatchException e){
						System.out.println("Silahkan Input Bilangan Riil");
						scanner.nextLine();
						continue;
					}	
				}while(true);
				
				System.out.println();
				for(int indexRow = 0; indexRow < 2; indexRow++){
					for(int indexColumn = 0; indexColumn < 2; indexColumn++){
						
						System.out.printf("%.2f  ", matrix2[indexRow][indexColumn]);
					}
					System.out.println();
				}
				
				double determinant2 = (matrix2[0][0]*matrix2[1][1])-(matrix2[1][0]*matrix2[0][1]);
				System.out.println("Determinan dari Matriks 2 adalah " + determinant2);
				if (determinant2 == 0) {
					System.out.println("Ini merupakan Matriks Pencerminan");
				}
				else {
					System.out.println("Matriks ini Bukanlah Matriks Pencerminan");
				}
			}while(true);
		
		}
	
	public static void main(String[] args){
		OOPMatrix call = new OOPMatrix();
		try{
			call.MainMenu();
		}catch(NoSuchElementException e){
			System.out.println("\n");
			System.out.println("You pressed Ctrl + Z");
			System.out.println("Program terminated");
		}		
    }	
}

class Matrix{
	double matrix1, matrix2, multiply;
		double add(){
			return matrix1 + matrix2;
	}
		double substract(){
			return matrix1 - matrix2;
		}
		double multiply(){
			return multiply;
		}
	

}