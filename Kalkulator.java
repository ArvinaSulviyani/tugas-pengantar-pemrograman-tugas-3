
import java.util.*;

public class Kalkulator {
		private double value_1;
		private double value_2;
		private double result;
	
		public void setValue1(double n){
		value_1 = n;
		}
	
		public double getValue1(){
			return value_1;
		}
	
		public void setValue2(double n){
		value_2 = n;
		}
	
		public double getValue2(){
			return value_2;
		}
	
		public double getResult(){
			return result;
		}
		
		public void AdditionResult(){
			result = value_1 + value_2;
		}
	
		public void ReductionResult(){
			result = value_1 - value_2;
		}
		
		public void MultiplyResult(){
			result = value_1 * value_2;
		}
		
		public void DivideResult(){
			result = value_1 / value_2;
		}
		
		public void RootResult(){
			result = Math.sqrt(value_1);
		}
		
		public void powerresult(){
			result = Math.pow(value_1, value_2);
		}

	}

	class KalkulatorMain{
		
		public static void main (String [] args){
			Kalkulator object = new Kalkulator();
			Scanner scanner = new Scanner(System.in);
			boolean kontinu = false;
			int choice;
			
			do{
				try{
				System.out.println("Kalkulator");
				System.out.println("1. Penjumlahan");
				System.out.println("2. Pengurangan");
				System.out.println("3. Perkalian");
				System.out.println("4. Pembagian");
				System.out.println("5. Akar");
				System.out.println("6. Pangkat");
				System.out.println("7. Exit");
				
				System.out.println("");
				System.out.print("Masukan Pilihan Anda : ");
				choice = scanner.nextInt();
				System.out.println("");
				
				
				if(choice==1){
					try{
					System.out.print("Masukan Bilangan Pertama : ");
					double value1 = scanner.nextDouble();
					object.setValue1(value1);
					System.out.println();
					
					System.out.print("Masukan Bilangan Kedua : ");
					double value2 = scanner.nextDouble();
					object.setValue2(value2);
					System.out.println();
					
					object.AdditionResult();
					System.out.println(value1 + " + " + value2 + " = " + object.getResult());
					System.out.println();
					}
					
					catch(InputMismatchException e){
					System.out.println("Inputan Anda Salah");
					System.out.println();
					scanner.nextLine();
					continue;	
					}
						
					catch (NoSuchElementException e){
					System.out.println("Inputan Anda Salah");
					}
					
				}
				
				else if (choice==2){
					try{
					System.out.print("Masukan Bilangan Pertama : ");
					double value1 = scanner.nextDouble();
					object.setValue1(value1);
					System.out.println();
						
					System.out.print("Masukan Bilangan Kedua : ");
					double value2 = scanner.nextDouble();
					object.setValue2(value2);
					System.out.println();
					
						
					
					object.ReductionResult();
					System.out.println(value1 + " - " + value2 + " = " + object.getResult());	
					System.out.println();
					}
					
					catch(InputMismatchException e){
					System.out.println("Inputan Anda Salah");
					System.out.println();
					scanner.nextLine();
					continue;	
					}
						
					catch (NoSuchElementException e){
					System.out.println("Inputan Anda Salah");
					}
					
				}
				
				else if(choice==3){
					try{
						System.out.print("Masukan Bilangan Pertama : ");
						double value1 = scanner.nextDouble();
						object.setValue1(value1);
						System.out.println();
						
						System.out.print("Masukan Bilangan Kedua : ");
						double value2 = scanner.nextDouble();
						object.setValue2(value2);
						System.out.println();
						
						object.MultiplyResult();
						System.out.println(value1 + " x " + value2 + " = " + object.getResult());	
						System.out.println();
						
					}
						
						catch(InputMismatchException e){
						System.out.println("Inputan Anda Salah");
						System.out.println();
						scanner.nextLine();
						continue;	
						}
							
						catch (NoSuchElementException e){
						System.out.println("Inputan Anda Salah");
						}
						
						
					}
				
				else if(choice==4){
					try{
						System.out.print("Masukan Bilangan Pertama : ");
						double value1 = scanner.nextDouble();
						object.setValue1(value1);
						System.out.println();
						
						System.out.print("Masukan Bilangan Kedua : ");
						double value2 = scanner.nextDouble();
						object.setValue2(value2);
						System.out.println();
						
						object.DivideResult();
						System.out.println(value1 + " : " + value2 + " = " + object.getResult());	
						System.out.println();
						
					}
						
						catch(InputMismatchException e){
						System.out.println("Inputan Anda Salah");
						System.out.println();
						scanner.nextLine();
						continue;	
						}
							
						catch (NoSuchElementException e){
						System.out.println("Inputan Anda Salah");
						}
						
					}
				
				else if(choice==5){
					try{
						System.out.println("Masukan Bilangan yang akan di akarkan : ");
						double value1 = scanner.nextDouble();
						object.setValue1(value1);
						System.out.println();
						
						object.RootResult();
						System.out.println("Akar dari " + value1 + " = " + object.getResult());	
						System.out.println();
					
					}
						
						catch(InputMismatchException e){
						System.out.println("Inputan Anda Salah");
						System.out.println();
						scanner.nextLine();
						continue;	
						}
							
						catch (NoSuchElementException e){
						System.out.println("Inputan Anda Salah");
						}
						
					}
				
				else if(choice==6){
					try{
						System.out.print("Masukan Bilangan : ");
						double value1 = scanner.nextDouble();
						object.setValue1(value1);
						System.out.println();
						
						System.out.print("Masukan Pangkat : ");
						double value2 = scanner.nextDouble();
						object.setValue2(value2);
						System.out.println();
						
						object.powerresult();
						System.out.println(value1 + " Pangkat " + value2 + " = " + object.getResult());	
						System.out.println();
					
					}
						
						catch(InputMismatchException e){
						System.out.println("Inputan Anda Salah");
						System.out.println();
						scanner.nextLine();
						continue;	
						}
							
						catch (NoSuchElementException e){
						System.out.println("Inputan Anda Salah");
						}
						
					}
				
				else if(choice==7){
					System.out.println("Terima Kasih");
					break;
					
				}
				
				else{
					System.out.println("Inputan anda salah");
					System.out.println();
				}
				}
				
				catch(InputMismatchException e){
				System.out.println("Inputan Anda Salah");
				System.out.println();
				scanner.nextLine();
				continue;	
				}
				
				catch (NoSuchElementException e){
					System.out.println("Inputan Anda Salah");
				}

				}while(kontinu=true);

		}

	}